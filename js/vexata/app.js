/*
 * 
 * @param {Object} 'app'
 * @param {Object} ['ngRoute'
 * @param {Object} 'kendo.directives']
 */

(function () {
   	'use strict';
	 vexataNameSpace.globalList.app = angular.module('app', ['ui.router','ngAnimate','kendo.directives']);
})();

function getTotalCount($http, url, cb){
  // Simple GET request example :
  $http.get(url).
  success(function(data, status, headers, config) {
  	cb(data.length)
    // this callback will be called asynchronously
    // when the response is available
  }).
  error(function(data, status, headers, config) {
    // called asynchronously if an error occurs
    // or server returns response with an error status.
  });
}

function tempChart(){
	$("#tempChart").kendoChart({
                title: {
                    text: "Temperature Report"
                },
                legend: {
                    position: "bottom"
                },
                series: [{
                    type: "line",
                    data: [6, 10, 10, 10, 10, 9, 5, 5, 10, 8, 8, 5, 8, 11, 9, 15, 20, 23, 24, 21, 21, 20, 22, 22, 20, 18, 16, 15, 20, 13.2, 18],
                    name: "30[&deg;C] to 50[&deg;C]",
                    color: "#ff1c1c"
                }, {
                    type: "line",
                    data: [-5, -6, 0, -4, -3, -5.2, -5, -1.7, -1, 0, -0.4, -2, -2, -5, 4, -2, -4, -1, -1, 2, 4, -1, 1, 1, 4, 0, -1, 1, -2, 5.7, 5],
                    name: "10[&deg;C] to 20[&deg;C]",
                    color: "#ffae00"
                }, {
                    type: "area",
                    data: [16.4, 21.7, 35.4, 19, 10.9, 13.6, 10.9, 10.9, 10.9, 16.4, 16.4, 13.6, 13.6, 29.9, 27.1, 16.4, 13.6, 10.9, 16.4, 10.9, 24.5, 10.9, 8.1, 19, 21.7, 27.1, 24.5, 16.4, 27.1, 29.9, 27.1],
                    name: "-5[&deg;C] to 10[&deg;C]",
                    color: "#73c100"
                }],
                valueAxes: [{
                    name: "high",
                    min: 0,
                    max: 50
                }],
                categoryAxis: {
                    categories: ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31"],
                    // Align the first two value axes to the left
                    // and the last two to the right.
                    //
                    // Right alignment is done by specifying a
                    // crossing value greater than or equal to
                    // the number of categories.
                    axisCrossingValues: [0, 10, 10],
                    justified: false
                },
                tooltip: {
                    visible: true,
                    format: "{0}",
                    template: "#= category #: #= value #"
                }
            });
			vexataNameSpace.globalList.tempChart = $("#tempChart").data("kendoChart");
}

function createChartCapacity(storageData) {
    $("#chart-capacity").kendoChart({
        title: {
            position: "bottom",
            visible: false
//            text: "Volume Capacity"
        },
        legend: {
            position: "top",
            visible: false
        },
        chartArea: {
            background: ""
        },
        seriesDefaults: {
            labels: {
                template: "#= category # - #= kendo.format('{0:P}', percentage)#",
                //position: "outsideEnd",
                visible: true,
                background: "transparent"
            }
        },
        series: [{
            type: "pie",
			nodeValue: storageData, //Add our own data/REST.
            startAngle: 0,
            data: [{
                category: "Used",
                value: storageData.used,
                color: "#428bca"				
            },/*{
                category: "Snapshots",
                value: 0.2,
                color: "#d9534f"
            },*/{
                category: "Free",
                value: storageData.free,
                color: "#5cb85c"
            }]
        }],
        tooltip: {
            visible: true,
			template: kendo.template($("#chartCapacity").html()), // Reffer: dashboard.html
            format: "{0}%"
        },
		
    });
    vexataNameSpace.globalList.createChartCapacity = $("#chart-capacity").data("kendoChart");
}

function logarthmiChart(){
	$("#logarthmiChart").kendoChart({
                title: {
                    text: "Node IOPS & Port Bandwidth",
					visible: false
                },
                legend: {
                    position: "top"
                },
                series: [{
                    type: "column",
                    data: [3530000, 153238000, 4540000, 1232000, 2542000,7528000, 6122000, 5459000, 7744000, 5126000, 5126000, 3530000],
                    stack: true,
                    name: "Read IOPS",
                    color: "#428bca"
                }, {
                    type: "column",
                    data: [15028000, 6122000, 5459000, 7744000, 5126000, 3530000, 6238000, 4540000, 1232000, 2542000, 4540000, 1232000],
                    stack: true,
                    name: "Write IOPS",
                    color: "#5cb85c"
                }, {
                    type: "area",
                    data: [7100, 6500, 4520, 2500, 14500, 25600, 25300, 9835, 25350, 14400, 9835, 25350],
                    name: "Read Bandwidth",
                    color: "#8A74C2",
                    axis: "rbn"
                }, {
                    type: "area",
                    data: [2560, 2530, 983, 2535, 1440, 7100, 6500, 4520, 2500, 1450, 1232, 2542],
                    name: "Write Bandwidth",
                    color: "#C29D4E",
                    axis: "rbn"
                }],
                valueAxes: [ {
                    title: { text: "IOPs Count" },
                    min: 0,
                    max: 8000000
                }, {
                    name: "rbn",
                    title: { text: "Bandwidth in MBps" },
                    color: "#8A74C2",
                    min: 0,
                    max: 32000
                }],
                categoryAxis: {
                    categories: ["00:05", "00:10", "00:15", "0:20", "0:25","0:30", "0:35", "0:40", "0:45","0:50", "0:55", "1:00"],
                    // Align the first two value axes to the left
                    // and the last two to the right.
                    //
                    // Right alignment is done by specifying a
                    // crossing value greater than or equal to
                    // the number of categories.
                    axisCrossingValues: [ 0, 20, 20]
                },
				tooltip: {
	                visible: true,
	                template: "Time: #= category # - Value: #= value #",
	                format: "{0}%"
	            }
            });  
	vexataNameSpace.globalList.logarthmiChart = $("#logarthmiChart").data("kendoChart");
}

function lineChart(title){

    var events = [{
        "size": "0",
        "win": 25,
        "event": 2
    },{
        "size": "1000",
        "win": 22,
        "event": 5
    },{
        "size": "2000",
        "win": 24,
        "event": 2
    },{
        "size": "3000",
        "win": 27,
        "eventMarker": "A",
        "event": 1
    },{
        "size": "4000",
        "win": 26,
        "event": 1
    },{
        "size": "5000",
        "win": 24,
        "event": 3
    },{
        "size": "6000",
        "win": 26,
        "event": 2
    },{
        "size": "7000",
        "win": 24,
        "eventMarker": "B",
        "event": 3
    },{
        "size": "4000",
        "win": 20,
        "event": 5
    },{
        "size": "9000",
        "win": 22,
        "event": 6
    },{
        "size": "8000",
        "win": 21,
        "event": 7
    },{
        "size": "7000",
        "win": 27,
        "event": 3
    }];

    $("#lineChart").kendoChart({
        dataSource: {
            data: events
        },
        title: {
            text: "Events in Last 1 Hour"
        },
        legend: {
            position: "bottom"
        },
        seriesDefaults: {
            type: "line"
        },
        series: [{
            field: "win",
            name: "Information",
         	color: "#428bca",
            notes: {
                label: {
                    position: "outside"
                },
                position: "bottom"
            }
        },{
            field: "event",
			color: "#5cb85c",
            name: "Critical Event",
            noteTextField: "eventMarker"
        }],
        valueAxis: {
            line: {
                visible: false
            }
        },
        categoryAxis: {
//            field: "size",
			categories: ["00:05", "00:10", "00:15", "0:20", "0:25","0:30", "0:35", "0:40", "0:45","0:50", "0:55", "1:00"],
			axisCrossingValues: [ 0, 20, 20],
            majorGridLines: {
                visible: false
            }
        },
        tooltip: {
            visible: true,
            template: "#= series.name #: #= value #"
        }
    });
	vexataNameSpace.globalList.lineChart = $("#lineChart").data("kendoChart");
}

function tooltipKendo(element, filter, height, width, position, content, autohide, func){
	$(element).kendoTooltip({
				autoHide: autohide,
                filter: filter,
                width: width,
				height: height,
                position: position,
                animation: {
                  open: {
                    effects: "fade:in",
                    duration: 500
                  },
                  close: {
                    effects: "fade:out",
                    duration: 500
                  }
                },
                content: content,
				show: function() {
        			if(typeof(func) == 'function') tempChart();
					else return false;
      			}
        });
}

