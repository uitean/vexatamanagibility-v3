
  var vexataURL   = createVexataURL();
  var alertTime = 3000;

vexataNameSpace.globalList.app.factory('Base64', function() {
    var keyStr = 'ABCDEFGHIJKLMNOP' +
        'QRSTUVWXYZabcdef' +
        'ghijklmnopqrstuv' +
        'wxyz0123456789+/' +
        '=';
    return {
        encode: function (input) {
            var output = "";
            var chr1, chr2, chr3 = "";
            var enc1, enc2, enc3, enc4 = "";
            var i = 0;

            do {
                chr1 = input.charCodeAt(i++);
                chr2 = input.charCodeAt(i++);
                chr3 = input.charCodeAt(i++);

                enc1 = chr1 >> 2;
                enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
                enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
                enc4 = chr3 & 63;

                if (isNaN(chr2)) {
                    enc3 = enc4 = 64;
                } else if (isNaN(chr3)) {
                    enc4 = 64;
                }

                output = output +
                keyStr.charAt(enc1) +
                keyStr.charAt(enc2) +
                keyStr.charAt(enc3) +
                keyStr.charAt(enc4);
                chr1 = chr2 = chr3 = "";
                enc1 = enc2 = enc3 = enc4 = "";
            } while (i < input.length);

            return output;
        },

        decode: function (input) {
            var output = "";
            var chr1, chr2, chr3 = "";
            var enc1, enc2, enc3, enc4 = "";
            var i = 0;

            // remove all characters that are not A-Z, a-z, 0-9, +, /, or =
            var base64test = /[^A-Za-z0-9\+\/\=]/g;
            if (base64test.exec(input)) {
                alert("There were invalid base64 characters in the input text.\n" +
                "Valid base64 characters are A-Z, a-z, 0-9, '+', '/',and '='\n" +
                "Expect errors in decoding.");
            }
            input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");

            do {
                enc1 = keyStr.indexOf(input.charAt(i++));
                enc2 = keyStr.indexOf(input.charAt(i++));
                enc3 = keyStr.indexOf(input.charAt(i++));
                enc4 = keyStr.indexOf(input.charAt(i++));

                chr1 = (enc1 << 2) | (enc2 >> 4);
                chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
                chr3 = ((enc3 & 3) << 6) | enc4;

                output = output + String.fromCharCode(chr1);

                if (enc3 != 64) {
                    output = output + String.fromCharCode(chr2);
                }
                if (enc4 != 64) {
                    output = output + String.fromCharCode(chr3);
                }

                chr1 = chr2 = chr3 = "";
                enc1 = enc2 = enc3 = enc4 = "";

            } while (i < input.length);

            return output;
        }
    };
});

vexataNameSpace.globalList.app.factory("user",function(){
        return {};
});
//Login controller
vexataNameSpace.globalList.app.controller("loginCtrl", function($window, $scope,$http, Base64, user){
    if(sessionStorage.user) $window.location.href = "#dashboard";
    else{
        $scope.user = user;
        var validator = $("#myform").kendoValidator().data("kendoValidator");
        $('#btn-login').unbind('click').bind('click', function(e){
            if(validator.validate()){
                makeRequest($window, $scope, $http, Base64);
            };
        });
    }
});

//Index controller
vexataNameSpace.globalList.app.controller("index", function($window, $scope, user){
    $scope.user = user;
    var events = vexataNameSpace.globalList.events; //Reffer: Static events taken from nampspace.js
    var alarms = vexataNameSpace.globalList.alarms; 
    $scope.user.notification = events;
    $scope.user.warning = $.grep(alarms, function(e){ return (e.severity == "Warning"); });
    $scope.user.critical = $.grep(alarms, function(e){ return (e.severity == "Critical"); });
    $scope.user.logout = function(){
            delete sessionStorage.user;
            vexataNameSpace.globalList.toolbars = ["excel", "pdf"];
            vexataNameSpace.globalList.editOpts = {};
    };
    if(sessionStorage.user){
        var userDetails = JSON.parse(sessionStorage.user);        
        vexataNameSpace.authkeys.value = userDetails.authkeys;
        if(userDetails.displayRole == "SAadmin") {
            vexataNameSpace.globalList.toolbars.unshift({name: "create",text: "Create New"});
            vexataNameSpace.globalList.editOpts = {command: ["edit", "destroy"], title: "&nbsp;", width: "200px"};
        }
        $scope.user.firstname = userDetails.firstname;                 
        $('.navbar').css('display', 'block');
    }
});
//Dashboard Controller
vexataNameSpace.globalList.app.controller("dashboard", function($window, $scope,$http, user){
        if(sessionStorage.user) $('.navbar').css('display', 'block');
        else $window.location.href = "#login";
        //Making request to get Storage Information
        vexataNameSpace.globalList.getData($http, vexataURL+vexataNameSpace.api.storageURL, function(data,err){
            if(data){
                $scope.storageInfo = data;
				createChartCapacity(data);
            }else{
                $scope.nodes = err
            }
        });
        //Loading dashboard charts
        logarthmiChart();
        lineChart();
		
		//DOM Declaration
		var panel = $('#slide-panel');
		
        $('.getTime').text(getTime());
		$(".panel").find("a").attr('href', 'javascript:void(0)');
		$('.iopBandwidthInterval li').on('click', function(){
			var hrData = ["00:05", "00:10", "00:15", "0:20", "0:25","0:30", "0:35", "0:40", "0:45","0:50", "0:55", "1:00"];
			var dayData = ["2:00", "4:00", "6:00", "8:00", "10:00", "12:00", "14:00", "16:00", "18:00", "20:00", "22:00", "24:00"];
			var weekData =["20/12", "21/12", "22/12", "23/12", "24/12", "25/12", "26/12", "27/12", "28/12", "29/12", "30/12", "31/12", "01/01", "02/01"];
			$('.iopBandwidthInterval').parent().find('button').html($(this).text()+'<span class="caret"></span>');
			var val = $(this).text().trim();
			if(val == "Last 1 Day"){
				vexataNameSpace.globalList.logarthmiChart.options.categoryAxis.categories = dayData;	
				vexataNameSpace.globalList.lineChart.options.categoryAxis.categories = dayData;
			}else if(val == "Last 2 Weeks"){
				vexataNameSpace.globalList.logarthmiChart.options.categoryAxis.categories = weekData;
				vexataNameSpace.globalList.lineChart.options.categoryAxis.categories = weekData;
			}else{
				vexataNameSpace.globalList.logarthmiChart.options.categoryAxis.categories = hrData;
				vexataNameSpace.globalList.lineChart.options.categoryAxis.categories = hrData;
			}
			vexataNameSpace.globalList.lineChart.options.title.text = "Events in "+$(this).text();
			vexataNameSpace.globalList.lineChart.refresh();
			vexataNameSpace.globalList.logarthmiChart.refresh();
		});
       		
		//Get the external template definition using a jQuery selector
        var fanTemplate = kendo.template($("#fanStatusTemplate").html());
		var batteryTemplate = kendo.template($("#baterryTemplate").html());
		var iocTemplate = kendo.template($("#iocTemplate").html());
        //Create some dummy data
//        var data = [{fanStatus: 'ERROR', speed: '70'},{fanStatus: 'FAILED', speed: '40'},{fanStatus: 'OK', speed: '56'},{fanStatus: 'OK', speed: '42'},{fanStatus: 'OK', speed: '38'}];
//        fanTemplate = fanTemplate(data); //Execute the template    

		$( window ).resize(function() { // Browser resize capture.
			vexataNameSpace.globalList.logarthmiChart.refresh();
			vexataNameSpace.globalList.lineChart.refresh();
			vexataNameSpace.globalList.createChartCapacity.refresh();
		});

		$scope.events = vexataNameSpace.globalList.alarms; //Reffer: Static events taken from nampspace.js
		
		//Making request to get Port Information
		vexataNameSpace.globalList.getData($http, vexataURL+vexataNameSpace.api.portsURL, function(data,err){
			if(data){
				$scope.ports = data
			}else{
				$scope.ports = err
			}
		});
		
		//Making request to get Node Information
		vexataNameSpace.globalList.getData($http, vexataURL+vexataNameSpace.api.nodeURL, function(data,err){
			if(data){
				var errorFans = $.grep(data.fans, function(e){ return (e.fanStatus == "ERROR" || e.fanStatus == "FAILED"); });
				var successFans = $.grep(data.fans, function(e){ return (e.fanStatus == "OK"); });
				$scope.trayOneErrorFans = $.grep(errorFans, function(e){return e.fanNumber <= 12 });
				$scope.trayOneOkFans = $.grep(successFans, function(e){return e.fanNumber <= 12 });
				$scope.trayTwoErrorFans = $.grep(errorFans, function(e){return e.fanNumber > 12 });
				$scope.trayTwoOkFans = $.grep(successFans, function(e){return e.fanNumber > 12 });
				$scope.batteryBackups = data.batteryBackups;
				$scope.esms = data.esms;
				$scope.fans = data.fans;
				$scope.powerSupplies = data.powerSupplies;
				$scope.ioControllers = data.ioControllers;
			}else{
				$scope.nodes = err
			}
		});

//		batteryTemplate = batteryTemplate(data);
		//Enable kendo tooltip   
		tooltipKendo("#agglomerations", "span", "auto", "200px", "top", kendo.template($("#template").html()), true);
		tooltipKendo("#fanCircle", "span", "300px", "285px", "left", fanTemplate, false);
		tooltipKendo("#esm-transition", "span", "auto", "180px", "left", kendo.template($("#esmTemplate").html()), true);
		tooltipKendo("#powerSupplies", "span", "auto", "220px", "left", kendo.template($("#powerSuppliesTemplate").html()), true);
		tooltipKendo("#tempGraph", "span", "auto", "750px", "left", kendo.template($("#temperatureTemplate").html()), true, tempChart);
		tooltipKendo("#baterry-backup", "span", "auto", "auto", "left", batteryTemplate, true);
		tooltipKendo("#ioControllers", "span", "auto", "220px", "left", iocTemplate, true);
		
	});

//Volume Controller
vexataNameSpace.globalList.app.controller("volume", function($window, $scope){
        if(!sessionStorage.user) $window.location.href = "#login";
		$scope.mainGridOptions = {
				autoBind: true,
				toolbar: vexataNameSpace.globalList.toolbars,
				excel: {
			        fileName: "Vexata Inc.xlsx"
			    },
				pdf: {
					fileName: "Vexata Inc.pdf"
				},
                dataSource: {
                    transport: {
                        read:  {
                            url: vexataURL+vexataNameSpace.api.volumeURL,
                            dataType: "json",
                            headers: {Authorization: vexataNameSpace.authkeys.value}
                        },
                        update: {
                            url: function(data){
								return vexataURL+vexataNameSpace.api.volumeURL + data.models[0].id
							},
                            contentType: "application/json",
							dataType: "json",
							type: "PUT",
                            headers: {Authorization: vexataNameSpace.authkeys.value}
                        },
                        destroy: {
                            url: function(data){
								return vexataURL+vexataNameSpace.api.volumeURL + data.models[0].id
							},
							type: "DELETE",
                            headers: {Authorization: vexataNameSpace.authkeys.value}
                        },
                        create: {
                            url: vexataURL+vexataNameSpace.api.volumeURL,
                            contentType: "application/json",
							dataType: "json",
							type: "POST",
                            headers: {Authorization: vexataNameSpace.authkeys.value}
                        },
                        parameterMap: function(options, operation) {
                            if (operation !== "read" && options.models && operation !== "destroy") {
                                return JSON.stringify(options.models[0]);
                            }
                        }
                    },
                    batch: true,
					schema: {
                        model: {
                            id: "id",
                            fields: {
                                id: { editable: false, type: "number", nullable: true },
                                name: { validation: { required: true } },
                                description: { nullable: true },
                                volSize: { type: "number", validation: { min: 32, required: true } }

	                            }
	                        }
	                },
					pageSize: 15
                },
                columns: [
                            { field:"name", title: "Name" },
                            { field: "description", title:"Description" },
                            { field: "volSize", title:"Size" },
                            vexataNameSpace.globalList.editOpts],
						editable: {mode: "popup", createAt: "top"},
						sortable: true,
						filterable: true,
						scrollable: false,
						pageable: true,
						groupable:{
							messages:{
								empty: "Drag and Drop Columns here"
							}
						}
            };
	});

//VolumeGroup Controller
vexataNameSpace.globalList.app.controller("volumegroup", function($window, $scope){
    if(!sessionStorage.user) $window.location.href = "#login";
    $scope.volumeGroupGridOptions = {
        autoBind: true,
        toolbar: vexataNameSpace.globalList.toolbars,
        excel: {
            fileName: "Vexata Inc.xlsx"
        },
        pdf: {
            fileName: "Vexata Inc.pdf"
        },
        dataSource: {
            transport: {
                read:  {
                    url: vexataURL+vexataNameSpace.api.volumegroupURL,
                    dataType: "json",
                    headers: {Authorization: vexataNameSpace.authkeys.value}
                },
                update: {
                    url: function(data){
                        return vexataURL+vexataNameSpace.api.volumegroupURL + data.models[0].id
                    },
                    contentType: "application/json",
                    dataType: "json",
                    type: "PUT",
                    headers: {Authorization: vexataNameSpace.authkeys.value}
                },
                destroy: {
                    url: function(data){
                        return vexataURL+vexataNameSpace.api.volumegroupURL + data.models[0].id
                    },
                    type: "DELETE",
                    headers: {Authorization: vexataNameSpace.authkeys.value}
                },
                create: {
                    url: vexataURL+vexataNameSpace.api.volumegroupURL,
                    contentType: "application/json",
                    dataType: "json",
                    type: "POST",
                    headers: {Authorization: vexataNameSpace.authkeys.value}

                },
                parameterMap: function(options, operation) {
                    if (operation !== "read" && options.models && operation !== "destroy") {
                        return JSON.stringify(options.models[0]);
                    }
                }
            },
            batch: true,
            schema: {
                model: {
                    id: "id",
                    fields: {
                        id: { editable: false, type: "number", nullable: true },
                        name: { validation: { required: true } },
                        description: { nullable: true },
                        numOfVols: { editable: true, type: "string", nullable: true },
                        currVolumes:{ editable: false, type: "string", nullable: true },
                        addVolumes: { editable: false, type: "string", nullable: true },
                        deleteVolumes: { editable: false, type: "string", nullable: true }
                    }
                }
            },
            pageSize: 15
        },
        columns: [
            { field:"id", title: "Volume Id" },
            { field:"name", title: "Name" },
            { field: "description", title:"Description" },
            { field: "numOfVols", title:"No of Volumes" },
            { field: "currVolumes", title:"Current Volumes" },
            { field: "addVolumes", title:"Added Volumes" },
            { field: "deleteVolumes", title:"Deleted Volumes" },
            vexataNameSpace.globalList.editOpts],
        editable: {mode: "inline", createAt: "bottom"},
        sortable: true,
        filterable: true,
        scrollable: false,
        pageable: true,
        groupable:{
            messages:{
                empty: "Drag and Drop Columns here"
            }
        }
    };
});

//InitiatorGroup Controller	
vexataNameSpace.globalList.app.controller("initiatorgroup", function($window, $scope){
	if(!sessionStorage.user) $window.location.href = "#login";
    $scope.initiatorGridOptions = {
				autoBind: true,
				toolbar: vexataNameSpace.globalList.toolbars,
				excel: {
			        fileName: "Vexata Inc.xlsx"
			    },
				pdf: {
					fileName: "Vexata Inc.pdf"
				},
                dataSource: {
                    transport: {
                        read:  {
                            url: vexataURL+vexataNameSpace.api.InitiatorURL,
                            dataType: "json",
                            headers: {Authorization: vexataNameSpace.authkeys.value}
                        },
                        update: {
                            url: function(data){
								return vexataURL+vexataNameSpace.api.InitiatorURL + data.models[0].id
							},
                            contentType: "application/json",
							dataType: "json",
							type: "PUT",
                            headers: {Authorization: vexataNameSpace.authkeys.value}
                        },
                        destroy: {
                            url: function(data){
								return vexataURL+vexataNameSpace.api.InitiatorURL + data.models[0].id
							},
							type: "DELETE",
                            headers: {Authorization: vexataNameSpace.authkeys.value}
                        },
                        create: {
                            url: vexataURL+vexataNameSpace.api.InitiatorURL,
                            contentType: "application/json",
							dataType: "json",
							type: "POST",
                            headers: {Authorization: vexataNameSpace.authkeys.value}
                        },
                        parameterMap: function(options, operation) {
                            if (operation !== "read" && options.models && operation !== "destroy") {
                                return JSON.stringify(options.models[0]);
                            }
                        }
                    },
                    batch: true,
					schema: {
                        model: {
                            id: "id",
                            fields: {
                                id: { editable: false, type: "number", nullable: true },
                                name: { validation: { required: true } },
                                description: { nullable: true },
                                currIGs: { type: "string", nullable: true },
                                addIGs: { type: "string", nullable: true },
								deleteIG: { type: "string", nullable: true }
	                            }
	                        }
	                },
					pageSize: 15
                },
                columns: [
							{ field:"id", title: "ID" },
                            { field:"name", title: "Name" },
                            { field: "description", title:"Description" },
                            { field: "currIGs", title:"currIGs" },
                            { field: "addIGs", title:"addIGs", width: "50px" },
							{ field: "deleteIG", title:"deleteIG", width: "60px" },
                            vexataNameSpace.globalList.editOpts],
						editable: {mode: "popup", createAt: "bottom"},
						sortable: true,
						filterable: true,
						scrollable: false,
						pageable: true,
						groupable:{
							messages:{
								empty: "Drag and Drop Columns here"
							}
						}
            };
});

//TargetGroup Controller	
vexataNameSpace.globalList.app.controller("targetgroup", function($window, $scope){
	
	$scope.targetGridOptions = {
				autoBind: true,
				toolbar: vexataNameSpace.globalList.toolbars,
				excel: {
			        fileName: "Vexata Inc.xlsx"
			    },
				pdf: {
					fileName: "Vexata Inc.pdf"
				},
                dataSource: {
                    transport: {
                        read:  {
                            url: vexataURL+vexataNameSpace.api.targetURL,
                            dataType: "json",
                            headers: {Authorization: vexataNameSpace.authkeys.value}
                        },
                        update: {
                            url: function(data){
								return vexataURL+vexataNameSpace.api.targetURL + data.models[0].id
							},
                            contentType: "application/json",
							dataType: "json",
							type: "PUT",
                            headers: {Authorization: vexataNameSpace.authkeys.value}
                        },
                        destroy: {
                            url: function(data){
								return vexataURL+vexataNameSpace.api.targetURL + data.models[0].id
							},
							type: "DELETE",
                            headers: {Authorization: vexataNameSpace.authkeys.value}
                        },
                        create: {
                            url: vexataURL+vexataNameSpace.api.targetURL,
                            contentType: "application/json",
							dataType: "json",
							type: "POST",
                            headers: {Authorization: vexataNameSpace.authkeys.value}
                        },
                        parameterMap: function(options, operation) {
                            if (operation !== "read" && options.models && operation !== "destroy") {
                                return JSON.stringify(options.models[0]);
                            }
                        }
                    },
                    batch: true,
					schema: {
                        model: {
                            id: "id",
                            fields: {
                                id: { editable: false, type: "number", nullable: true },
                                name: { validation: { required: true } },
                                description: { nullable: true },
                                currTGs: { type: "string", nullable: true },
                                addTGs: { type: "string", nullable: true },
								deleteTG: { type: "string", nullable: true }
	                            }
	                        }
	                },
					pageSize: 7
                },
                columns: [
							{ field:"id", title: "ID" },
                            { field:"name", title: "Name" },
                            { field: "description", title:"Description" },
                            { field: "currTGs", title:"Current Initiators List" },
                            { field: "addTGs", title:"New Initiators List", width: "50px" },
							{ field: "deleteTG", title:"Delete Initiators List", width: "60px" },
                            vexataNameSpace.globalList.editOpts],
						editable: true,
						sortable: true,
						filterable: true,
						scrollable: false,
						pageable: true,
                        toolbar: ["create","save","cancel"],
        				groupable:{
							messages:{
								empty: "Drag and Drop Columns here"
							}
						}
            };
});

//ExportGroup Controller
vexataNameSpace.globalList.app.controller("exportgroup", function($window, $scope){

    $scope.exportGridOptions = {
	        autoBind: true,
	        toolbar: vexataNameSpace.globalList.toolbars,
			excel: {
		        fileName: "Vexata Inc.xlsx"
		    },
			pdf: {
				fileName: "Vexata Inc.pdf"
			},
        	dataSource: {
            transport: {
                read:  {
                    url: vexataURL+vexataNameSpace.api.exportURL,
                    dataType: "json",
                    headers: {Authorization: vexataNameSpace.authkeys.value}
                },
                update: {
                    url: function(data){
                        return vexataURL+vexataNameSpace.api.exportURL + data.models[0].id
                    },
                    contentType: "application/json",
                    dataType: "json",
                    type: "PUT",
                    headers: {Authorization: vexataNameSpace.authkeys.value}
                },
                destroy: {
                    url: function(data){
                        return vexataURL+vexataNameSpace.api.exportURL + data.models[0].id
                    },
                    type: "DELETE",
                    headers: {Authorization: vexataNameSpace.authkeys.value}
                },
                create: {
                    url: vexataURL+vexataNameSpace.api.exportURL,
                    contentType: "application/json",
                    dataType: "json",
                    type: "POST",
                    headers: {Authorization: vexataNameSpace.authkeys.value}
                },
                parameterMap: function(options, operation) {
                    if (operation !== "read" && options.models && operation !== "destroy") {
                        return JSON.stringify(options.models[0]);
                    }
                }
            },
            batch: true,
            schema: {
                model: {
                    id: "id",
                    fields: {
                        id: { editable: false, type: "number", nullable: true },
                        name: { validation: { required: true } },
                        description: { nullable: true },
                        currEGs: { type: "string", nullable: true },
                        addEGs: { type: "string", nullable: true },
                        deleteEG: { type: "string", nullable: true }
                    }
                }
            },
            pageSize: 15
        },
        columns: [
            { field:"id", title: "ID" },
            { field:"name", title: "Name" },
            { field: "description", title:"Description" },
            { field: "currEGs", title:"currEGs" },
            { field: "addEGs", title:"addEGs", width: "50px" },
            { field: "deleteEG", title:"deleteEG", width: "60px" },
            vexataNameSpace.globalList.editOpts],
        editable: {mode: "inline", createAt: "top"},
		sortable: true,
		filterable: true,
		scrollable: false,
		pageable: true,
        groupable:{
            messages:{
                empty: "Drag and Drop Columns here"
            }
        }
    };
});

//Logical Asset Controller
vexataNameSpace.globalList.app.controller("logicalasset", function($window, $scope,$http){
    if(!sessionStorage.user) $window.location.href = "#login";
    //Count
    getTotalCount($http, vexataURL+vexataNameSpace.api.volumeURL, function(data){
        $(".volume").text(data);
    });
    getTotalCount($http, vexataURL+vexataNameSpace.api.volumegroupURL, function(data){
        $(".volumeGP").text(data);
    });
    getTotalCount($http, vexataURL+vexataNameSpace.api.InitiatorURL, function(data){
        $(".initiatorGP").text(data);
    });
    getTotalCount($http, vexataURL+vexataNameSpace.api.targetURL, function(data){
        $(".targetGP").text(data);
    });
    getTotalCount($http, vexataURL+vexataNameSpace.api.exportURL, function(data){
        $(".exportGP").text(data);
    });
});

function getTime() {
    var months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
    var days = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
        var d = new Date();
    var day = days[d.getDay()];
    var hr = d.getHours();
    var min = d.getMinutes();
    if (min < 10) {
        min = "0" + min;
    }
    var ampm = hr < 12 ? "AM" : "PM";
    var date = d.getDate();
    var month = months[d.getMonth()];
    var year = d.getFullYear();
    var x = document.getElementById("demo");
    var time = day + " " + hr + ":" + min + ampm + " " + date + "-" + month + "-" + year;
    return time;
}

function makeRequest($window, $scope, $http, Base64) {
    var base64Encode = 'Basic ' + Base64.encode($('#login-username').val() + ':' + $('#login-password').val());
    $http.defaults.headers.common = {"Access-Control-Request-Headers": "accept, origin, authorization"}; //you probably don't need this line.  This lets me connect to my server on a different domain
    $http.defaults.headers.common['Authorization'] = base64Encode;
    var user = {};
    $http({method: 'GET', url: vexataURL+'/api/users/login'}).
        success(function(data, status, headers, config) {
            $scope.user.firstname = data.username; 
            vexataNameSpace.authkeys.value = base64Encode;
            user.firstname = data.username; 
            user.authkeys = base64Encode;
            user.displayRole = data.displayRole;
            if(data.displayRole == "SAadmin") {
                vexataNameSpace.globalList.toolbars.unshift({name: "create",text: "Create New"});
                vexataNameSpace.globalList.editOpts = {command: ["edit", "destroy"], title: "&nbsp;", width: "200px"};
            }
            sessionStorage.user = JSON.stringify(user);
            $window.location.href = "#dashboard";
        }).
        error(function(data, status, headers, config) {
            $('.alert-danger').show("slow");
            $scope.validationErrorMessage  = "Invalid username or password";
            setTimeout(function(){
                $('.alert-danger').hide("slow");
            }, alertTime);
            // called asynchronously if an error occurs
            // or server returns response with an error status.
        });
}

function createVexataURL(){
    if(location.protocol === "file:" || location.protocol === "http:"){
        vexataURL = vexataNameSpace.config.protocol.http+vexataNameSpace.config.host+":"+vexataNameSpace.config.port;
    }
    else{
        vexataURL = vexataNameSpace.config.protocol.https +vexataNameSpace.config.host+":"+vexataNameSpace.config.port;
    }
    return vexataURL;
}
