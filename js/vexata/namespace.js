/*
 * Purpose: Namespace for Vexata
 * Date: 23-Jan-2015
 * Version: Initial Version
 * � 2015 Vexata Inc
 */
if (typeof vexataNameSpace == "undefined") {
    var vexataNameSpace = {
        config: {
            host: "localhost",
            port: "9000",
            protocol: {
                https: "https://",
                http: "http://"
            }
        },
        utilities: {
            templateURL: "",
            createTemplateURL: function(){
                if(location.protocol === "file:"){
                    vexataNameSpace.utilities.templateURL = "file://" +window.location.pathname.split("index.html")[0]+"views/"
                }
                else if(location.protocol === "http:")
                {
                    vexataNameSpace.utilities.templateURL = "http://" +location.host+window.location.pathname.split("index.html")[0]+"views/"
                }
                else
                {
                    vexataNameSpace.utilities.templateURL = "https://" +location.host+window.location.pathname.split("index.html")[0]+"views/"
                }
                return vexataNameSpace.utilities.templateURL;
            }
        },
        authkeys:{
          key:'Authorization',
          value: null
        },
        api:{
            volumeURL: "/api/volumes/",
            volumegroupURL: "/api/volumeGroups/",
            InitiatorURL: "/api/initiatorGroups/",
            targetURL: "/api/targetGroups/",
            exportURL: "/api/exportGroups/",
			portsURL: "/api/nodes/1/ports",
			nodeURL: "/api/nodes/1",
			storageURL: "/api/nodes/1/nodeLogicalInventory"
        },
        globalList: {
			
			getData: function(http, URL, callback){
				http.get(URL)
					.success(function(response){
						callback(response, null);
					})
					.error(function(error){
						callback(null, error)
					})
			},
			toolbars: ["excel", "pdf"],
			editOpts: { },
			events: [
			    {
			        "id": 1,
			        "severity": "information",
			        "category": "audit",
			        "entities": "volume",
			        "description": "volume: vol123 of size 12GB created",
			        "eventuuid": "152d0bc5-f9d9-4d7e-b505399b199316e2",
			        "modifiedBy": "admin",
			        "occuredat": "4 minutes ago"
			    },
				    {
			        "id": 2,
			        "severity": "information",
			        "category": "audit",
			        "entities": "snapshot",
			        "description": "volume: oracleDB snapshot created",
			        "eventuuid": "152d0bc5-f9d9-4d7e-1505399b199316e2",
			        "modifiedBy": "admin",
			        "occuredat": "12 minutes ago"
			    },
					    {
			        "id": 3,
			        "severity": "information",
			        "category": "task",
			        "entities": "clone",
			        "description": "volume: oracleDB Full Clone started",
			        "eventuuid": "252d0bc5-f9d9-4d7e-1505399b199316e2",
			        "modifiedBy": "admin",
			        "occuredat": "20 minutes ago"
			    },
					    {
			        "id": 4,
			        "severity": "information",
			        "category": "audit",
			        "entities": "initator Group",
			        "description": "Initator Group: HostEsxi-22 deleted",
			        "eventuuid": "252d0bc5-f9d9-4d7e-1505399b199316e2",
			        "modifiedBy": "admin",
			        "occuredat": "22 minutes ago"
			    },
						    {
			        "id": 5,
			        "severity": "information",
			        "category": "task",
			        "entities": "clone",
			        "description": "volume: oracleDB Full Clone completed",
			        "eventuuid": "252d0bc5-f9d9-4d7e-1505399b199316e2",
			        "modifiedBy": "admin",
			        "occuredat": "25 minutes ago"
			    }
			],
			alarms: [
			    {
			        "id": 3,
			        "severity": "Critical",
			        "category": "Logical",
			        "entities": "Volume",
                    "marker"  : "-",
			        "description": "Volume: MS SQL reached 90% capacity",
			        "alarmuuid": "c52d0bc5-f9d9-4d7e-b505399b699316e4",
			        "acknowledged": "false",
			        "cleared": "false",
			        "occuredat": "14 minutes ago",
			        "ackowledgedat": "timestamp",
			        "clearedat": "timestamp"
			    },
				{
			        "id": 9,
			        "severity": "Critical",
			        "category": "Logical",
			        "entities": "Volume",
                    "marker"  : "-",
                    "description": "Volume: MS SQL reached 70% capacity",
			        "alarmuuid": "c52d0bc5-f9d9-4d7e-b505399b699316e4",
			        "acknowledged": "false",
			        "cleared": "false",
			        "occuredat": "40 minutes ago",
			        "ackowledgedat": "timestamp",
			        "clearedat": "timestamp"
			    },
			    {
			        "id": 4,
			        "severity": "Warning",
			        "category": "Logical",
			        "entities": "Volume",
                    "marker"  : "A",
                    "description": "Volume: Oracle HR reached 80% capacity",
			        "alarmuuid": "c52d0bc5-f9d9-4d7e-b505399b699316e3",
			        "acknowledged": "false",
			        "cleared": "false",
			        "occuredat": "4 minutes ago",
			        "ackowledgedat": "timestamp",
			        "clearedat": "timestamp"
			    },
				{
			        "id": 10,
			        "severity": "Warning",
			        "category": "Logical",
			        "entities": "Volume",
                    "marker"  : "-",
                    "description": "Volume: Oracle HR reached 70% capacity",
			        "alarmuuid": "c52d0bc5-f9d9-4d7e-b505399b699316e3",
			        "acknowledged": "false",
			        "cleared": "false",
			        "occuredat": "24 minutes ago",
			        "ackowledgedat": "timestamp",
			        "clearedat": "timestamp"
			    },
			    {
			        "id": 5,
			        "severity": "Critical",
			        "category": "hardware",
			        "entities": "Fan",
                    "marker"  : "-",
                    "description": "Fan Tray 1, location 4 Failed",
			        "alarmuuid": "c52d0bc5-f9d9-4d7e-b505399ba99316e2",
			        "acknowledged": "false",
			        "cleared": "false",
			        "occuredat": "34 minutes ago",
			        "ackowledgedat": "timestamp",
			        "clearedat": "timestamp"
			    },
			    {
			        "id": 5,
			        "severity": "warning",
			        "category": "security",
			        "entities": "User",
                    "marker"  : "B",
                    "description": "User: abcuser Login attempt failed from IP Address 10.1.1.2, 3 times",
			        "alarmuuid": "c52d0bc5-f9d9-4d7e-b505399b199316e2",
			        "acknowledged": "false",
			        "cleared": "false",
			        "occuredat": "54 minutes ago",
			        "ackowledgedat": "timestamp",
			        "clearedat": "timestamp"
			    }
			],
			RoleBaseAccess: "Admin"
		}
    };
};
